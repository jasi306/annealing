﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AI_Projekt1
{
   
    class Annealing
    {
        int iter = 100000;
        static Random rand = new Random();
        private int p;
        private int j;
        private int r;

        public Annealing(int p, int j, int r)
        {
            this.p = p;
            this.j = j;
            this.r = r;

        }

        private int scanInt()
        {
            int temp = int.Parse(Console.ReadLine());
            //Console.WriteLine("'" + temp + "'");


            return temp;
        }
        private void getInput(out int[,] jobsTime, out List<List<int>> relations)
        {
            using (StreamReader file = new StreamReader("jobs.txt"))
            {
                jobsTime = new int[j, p];
                for (int i = 0; i < j; ++i)
                    for (int k = 0; k < p; ++k)
                    {
                        jobsTime[i, k] = int.Parse(file.ReadLine()); //scanInt();
                    }
            }

            using (StreamReader file = new StreamReader("relations.txt"))
            {
                relations = new List<List<int>>();
                for (int i = 0; i < j; ++i)
                    relations.Add(new List<int>());
                for (int i = 0; i < r; i++)
                {
                    int temp = int.Parse(file.ReadLine());//scanInt();
                    relations[int.Parse(file.ReadLine())/*scanInt()*/].Add(temp);
                }
            }


        }
        //private void getInput(out int[,] jobsTime, out List<List<int>> relations)
        //{

        //    jobsTime = new int[j, p];

        //    //int[,] temp = new int[r, 2];

        //    for (int i = 0; i < j; ++i)
        //        for (int k = 0; k < p; ++k)
        //        {
        //            //Console.WriteLine(i + " " + k+" ----");
        //            jobsTime[i, k] = scanInt();
        //        }



        //    relations = new List<List<int>>();
        //    for (int i = 0; i < j; ++i)
        //        relations.Add(new List<int>());
        //    for (int i = 0; i < r; i++)
        //    {
        //        int temp = scanInt();
        //        relations[scanInt()].Add(temp);
        //    }
        //}
        private List<List<int>> copy(List<List<int>> input)
        {
            List<List<int>> output = new List<List<int>>(input);
            for (int i = 0; i < output.Count; ++i)
            {
                output[i] = new List<int>(input[i]);
            }
            return output;
        }
        public void start()
        {
            if (p == 1 || j == 1)
            {
                throw new Exception("p == 1 || j == 1");
            }


            

            List<List<int>> assignmentQueuesToProcessors = new List<List<int>>();
            List<List<int>> bestAssignment = new List<List<int>>();
            List<List<int>> localBestAssignment = new List<List<int>>();

            int assignmentQueuesToProcessorsVal;
            int bestAssignmentVal;
            int localBestAssignmentVal;

            int[,] jobsTime;
            List<List<int>> relations;
            getInput(out jobsTime, out relations);

            for (int i = 0; i < p; ++i)
            {
                assignmentQueuesToProcessors.Add(new List<int>());
                bestAssignment.Add(new List<int>());
                localBestAssignment.Add(new List<int>());
            }


            getStartingAssignment(assignmentQueuesToProcessors);

            //localBestAssignment = copy(assignmentQueuesToProcessors);
            bestAssignment = copy(assignmentQueuesToProcessors);

            assignmentQueuesToProcessorsVal = bestAssignmentVal = localBestAssignmentVal = calcTime(assignmentQueuesToProcessors, relations, jobsTime);
            #region MainLoop
            for (int i = 0; i < iter; ++i)
            {
                int heat = (int)(j * (1 - ((float)i / iter)));

                randomizeReku(assignmentQueuesToProcessors, heat);
                assignmentQueuesToProcessorsVal = calcTime(assignmentQueuesToProcessors, relations, jobsTime);

                if (localBestAssignmentVal > assignmentQueuesToProcessorsVal)
                {
                    //localBestAssignment = copy(assignmentQueuesToProcessors);
                    localBestAssignmentVal = assignmentQueuesToProcessorsVal;
                    if (localBestAssignmentVal < bestAssignmentVal)
                    {
                        bestAssignment = copy(assignmentQueuesToProcessors);
                        bestAssignmentVal = localBestAssignmentVal;

                        Console.WriteLine("iteration {0} best {1}", i, bestAssignmentVal);

                    }
                }
                else
                {
                    if (AC(assignmentQueuesToProcessorsVal, localBestAssignmentVal, heat) > rand.NextDouble())
                    {
                        //localBestAssignment = copy(assignmentQueuesToProcessors);
                        localBestAssignmentVal = assignmentQueuesToProcessorsVal;
                    }
                }
            }
            #endregion
            Console.WriteLine();
            Console.WriteLine("best = {0}", bestAssignmentVal);
            Console.WriteLine();
            for (int i = 0; i < p; ++i)
            {
                Console.WriteLine("Processorowi {0} przypisano zadania: ",i);
                foreach (var job in bestAssignment[i])
                {
                    Console.Write(job + ",");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("dla tablicy czasow:");
            Console.WriteLine();
            Console.WriteLine("       numer procesora");
            Console.WriteLine("          |0|1|2|3|");
            Console.WriteLine("----------|-|-|-|-|");

            for (int i = 0; i < j; ++i)
            {
                Console.Write("Praca " + i.ToString("D3") + " |");
                for (int k = 0; k < p; ++k)
                {
                    Console.Write(jobsTime[i, k] + "|");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("I tablicy zaleznosci:");
            for (int i = 0; i < relations.Count(); ++i)
            {
                if (relations[i].Count() == 0) continue;
                if (relations[i].Count() == 1)
                    Console.Write("Przed zadaniem {0} należy wykonać zadanie: ", i);
                else
                    Console.Write("Przed zadaniem {0} należy wykonać zadania: ", i);
                for (int k = 0; k < relations[i].Count(); ++k)
                {
                    Console.Write("{0},", relations[i][k]);
                }
                Console.WriteLine();
            }





        }
        private void getStartingAssignment(List<List<int>> list)
        {
            for (int i = 0; i < j; ++i)
            {
                int procesor = rand.Next(list.Count); ;
                int job = rand.Next(list[procesor].Count);
                list[procesor].Insert(job, i);
            }
        }
        private List<List<int>> randomizeReku(List<List<int>> list, int heat)
        {
            if (heat == 0) return list;
            int procesor = rand.Next(list.Count);
            if (list[procesor].Count == 0)
            {
                return randomizeReku(list, heat - 1);
            }
            int job = rand.Next(list[procesor].Count);
            int element = list[procesor][job];
            list[procesor].RemoveAt(job);

            randomizeReku(list, heat - 1);

            int newProcesor = rand.Next(list.Count);
            /*while (newProcesor == procesor) */
            newProcesor = rand.Next(list.Count);
            int newJob = rand.Next(list[procesor].Count);
            /*while (newJob == job) */
            newJob = rand.Next(list[newProcesor].Count);
            try
            {
                list[newProcesor].Insert(newJob, element);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(list[newProcesor].Count() + " " + newJob);
            }

            return list;
        }
        private double AC(double first, double sec, int heat)
        {
            return Math.Exp((first - sec) / heat);
        }



        private int calcTime(List<List<int>> Assigments, List<List<int>> dep, int[,] jobsTime)
        {



            //assert(Assigments.size() == j);

            int SumTime = 0;

            List<bool> czyUkonczone = new bool[j + 1].ToList(); //1 dodatkowy na init
            List<bool> czyProcesorSkonczyl = new bool[p].ToList();
            List<int> tablica_czasu = new int[p].ToList();
            List<Queue<int>> jobsByProcessor = new Queue<int>[p].ToList();//= Assigments.Select(e => (new int[] { j }.ToList()).Concat(e).ToList()  )).Select(e => new Queue<int>(e)).ToList();// new List<Queue<int>>(r);
            for (int i = 0; i < p; ++i)
                jobsByProcessor[i] = new Queue<int>();



            for (int i = 0; i < p; ++i)
            {
                jobsByProcessor[i].Enqueue(j);
                foreach (var item in Assigments[i])
                {
                    jobsByProcessor[i].Enqueue(item);
                }
            }





            while (czyProcesorSkonczyl.Contains(false))
            {

                for (int i = 0; i < p; ++i)
                {

                    if (tablica_czasu[i] == 0 && !czyProcesorSkonczyl[i])
                    {

                        czyUkonczone[jobsByProcessor[i].Dequeue()] = true;


                        if (jobsByProcessor[i].Count() == 0)
                        {
                            czyProcesorSkonczyl[i] = true;
                            continue;
                        }
                    }
                    //}
                }
                //przydzial nowych zadan
                for (int i = 0; i < p; ++i)
                {
                    if (!czyProcesorSkonczyl[i])
                    {
                        if (tablica_czasu[i] <= 0)
                        {
                            if (sprawdzCzyMogeWziac(jobsByProcessor[i].Peek(), dep, czyUkonczone))
                            {
                                tablica_czasu[i] = jobsTime[jobsByProcessor[i].Peek(), i];//moze brac inny niz 1 z koleiki?
                                                                                          //<><><><><><><><><><><><><><><><><><><><><><><><><><
                                                                                          //cout << "procesor " << i << " wzial zadanie " << jobsByProcessor[i].front() << "o czasie wykonania " << j[jobsByProcessor[i].front()][i] << " w ticku: " << SumTime << endl;
                            }
                        }
                    }
                }


                //przesuniecie w czasie.

                int min = tablica_czasu.Select(a => a <= 0 ? int.MaxValue : a).Min();
                if (min == int.MaxValue)
                {
                    if (czyProcesorSkonczyl.Contains(false))
                    {
                        /*for (int i = 0; i < 1; ++i) */ //Console.WriteLine("can complite");
                        return int.MaxValue;
                    }
                    else
                    {
                        return SumTime;
                    }
                }
                /*int min = *min_element(
                    tablica_czasu.begin(),
                    tablica_czasu.end(),
                    [](int a, int b)//<><><><><><><>><<><><><>><><><><><<>><><<><>><><<>&?
                    {
                        return ((a <= 0) ^ (b <= 0) || a < b); //mniejszerowne zero nie sa najmniejsze.//a <= 0 || b <= 0 || a < b
                                                    //szukam najmniejszego niezerowego.
                    }
                );*/
                for (int i = 0; i < p; ++i)
                {
                    tablica_czasu[i] -= min;
                }
                SumTime += min;
            }
            return SumTime;
        }
        bool sprawdzCzyMogeWziac(int id, List<List<int>> dep, List<bool> ukonczone)
        {
            bool czymoge = true;
            foreach (var v in dep[id])
                if (ukonczone[v] == false)
                    return false;

            return czymoge;
        }

    };
}

